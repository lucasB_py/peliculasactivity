package com.trash.peliculasactivity.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.repositories.PeliculaRepository;

import java.util.List;

public class PeliculaListaViewModel extends ViewModel {

    private PeliculaRepository peliculaRepository;

    //Constructor
    public PeliculaListaViewModel() {
        peliculaRepository = PeliculaRepository.getInstance();
    }

    public LiveData<List<PeliculaModelo>> getPeliculas(){
        return peliculaRepository.getPeliculas();
    }

    public LiveData<List<PeliculaModelo>> getPeliculasPop(){
        return peliculaRepository.getPeliculasPop();
    }

    //Llamando al metodo in View - Model
    public void buscarPeliculaApi(String query, int pageNumber){
        peliculaRepository.buscarPeliculaApi(query, pageNumber);
    }

    public void buscarPeliculaPopApi(int pageNumber){
        peliculaRepository.buscarPeliculaPopApi(pageNumber);
    }

    public void buscarSiguientePagina(){
        peliculaRepository.buscarSiguientePagina();
    }
}
