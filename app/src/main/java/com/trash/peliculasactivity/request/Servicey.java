package com.trash.peliculasactivity.request;

import com.trash.peliculasactivity.utils.Credenciales;
import com.trash.peliculasactivity.utils.PeliculaAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Servicey {

    private static Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl(Credenciales.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static PeliculaAPI peliculaAPI = retrofit.create(PeliculaAPI.class);

    public static PeliculaAPI getPeliculaAPI() {
        return peliculaAPI;
    }
}
