package com.trash.peliculasactivity.request;


import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.trash.peliculasactivity.AppEjecutores;
import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.response.BuscarPeliculaResponse;
import com.trash.peliculasactivity.utils.Credenciales;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

public class PeliculaApiClient {

    //Livedata para la busqueda
    private MutableLiveData<List<PeliculaModelo>> mPeliculas;
    private MutableLiveData<List<PeliculaModelo>> mPeliculasPop;
    private static PeliculaApiClient instance;

    //Global Runnaable
    private RetrievePeliculasRunnable retrievePeliculasRunnable;
    private RetrievePeliculasRunnablePop retrievePeliculasRunnablePop;

    public static  PeliculaApiClient getInstance(){
        if (instance == null){
            instance = new PeliculaApiClient();
        }
        return instance;
    }

    private PeliculaApiClient(){
        mPeliculas = new MutableLiveData<>();
        mPeliculasPop = new MutableLiveData<>();
    }


    public LiveData<List<PeliculaModelo>> getPeliculas(){
        return mPeliculas;
    }
    public LiveData<List<PeliculaModelo>> getPeliculasPop(){
        return mPeliculasPop;
    }

    //El metodo buscarPeliculasApi, se llamara a traves de otras clases.
    public void buscarPeliculasApi(String query, int pageNumber){

        if (retrievePeliculasRunnable != null){
            retrievePeliculasRunnable = null;
        }

        retrievePeliculasRunnable = new RetrievePeliculasRunnable(query, pageNumber);

        final Future myHandler = AppEjecutores.getInstance().networkIO().submit(retrievePeliculasRunnable);

        AppEjecutores.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //  Cancela llamadas de retrofit
                myHandler.cancel(true);
            }
        }, 3000, TimeUnit.MILLISECONDS);
    }

    public void buscarPeliculasPopApi(int pageNumber){

        if (retrievePeliculasRunnablePop != null){
            retrievePeliculasRunnablePop = null;
        }

        retrievePeliculasRunnablePop = new RetrievePeliculasRunnablePop(pageNumber);

        final Future myHandler2 = AppEjecutores.getInstance().networkIO().submit(retrievePeliculasRunnablePop);

        AppEjecutores.getInstance().networkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //  Cancela llamadas de retrofit
                myHandler2.cancel(true);
            }
        }, 1000, TimeUnit.MILLISECONDS);
    }

    //Obteniendo data de la RestApi
    private class RetrievePeliculasRunnable implements Runnable{

        private String query;
        private int pageNumber;
        boolean cancelRequest;

        public RetrievePeliculasRunnable(String query, int pageNumber) {
            this.query = query;
            this.pageNumber = pageNumber;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response response = getPeliculas(query, pageNumber).execute();

                if (cancelRequest){
                    return;
                }
                if (response.code() == 200){
                    List<PeliculaModelo> lista = new ArrayList<>(((BuscarPeliculaResponse) response.body()).getPeliculas());
                    if (pageNumber == 1) {
                        mPeliculas.postValue(lista);

                    } else {

                        List<PeliculaModelo> peliculasActuales = mPeliculas.getValue();
                        peliculasActuales.addAll(lista);
                        mPeliculas.postValue(peliculasActuales);
                    }
                } else {
                    String error = response.errorBody().string();
                    Log.v("TAG", "ERROR: " + error);
                    mPeliculas.postValue(null);
                }

            } catch (IOException e){
                e.printStackTrace();
                mPeliculas.postValue(null);
            }
        }

        private Call<BuscarPeliculaResponse> getPeliculas(String query, int pageNumber){
            return Servicey.getPeliculaAPI().buscarPelicula(
                    Credenciales.API_KEY,
                    query,
                    pageNumber
            );
        }

        private void cancelRequest(){
            Log.v("TAG", "Cancelando solicitud de busqueda");
            cancelRequest = true;
        }
    }

    private class RetrievePeliculasRunnablePop implements Runnable{

        private int pageNumber;
        boolean cancelRequest;

        public RetrievePeliculasRunnablePop(int pageNumber) {
            this.pageNumber = pageNumber;
            cancelRequest = false;
        }

        @Override
        public void run() {
            try {
                Response response2 = getPeliculasPopulares(pageNumber).execute();
                if (cancelRequest){
                    return;
                }
                if (response2.code() == 200){
                    List<PeliculaModelo> lista = new ArrayList<>(((BuscarPeliculaResponse) response2.body()).getPeliculas());
                    if (pageNumber == 1) {
                        mPeliculasPop.postValue(lista);

                    } else {
                        List<PeliculaModelo> peliculasActuales = mPeliculasPop.getValue();
                        peliculasActuales.addAll(lista);
                        mPeliculasPop.postValue(peliculasActuales);
                    }
                } else {
                    String error = response2.errorBody().string();
                    Log.v("TAG", "ERROR: " + error);
                    mPeliculasPop.postValue(null);
                }

            } catch (IOException e){
                e.printStackTrace();
                mPeliculasPop.postValue(null);
            }
        }

        private Call<BuscarPeliculaResponse> getPeliculasPopulares(int pageNumber){
            return Servicey.getPeliculaAPI().getPopular(
                    Credenciales.API_KEY,
                    pageNumber
            );
        }

        private void cancelRequest(){
            Log.v("TAG", "Cancelando solicitud.");
            cancelRequest = true;
        }
    }
}
