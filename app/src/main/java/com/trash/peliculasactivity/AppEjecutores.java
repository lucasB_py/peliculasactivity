package com.trash.peliculasactivity;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class AppEjecutores {

    private static AppEjecutores instance;

    public static AppEjecutores getInstance(){
        if (instance == null){
            instance = new AppEjecutores();
        }
        return instance;
    }

    private final ScheduledExecutorService mNetworkIO = Executors.newScheduledThreadPool(3);

    public ScheduledExecutorService networkIO(){
        return mNetworkIO;
    }
}
