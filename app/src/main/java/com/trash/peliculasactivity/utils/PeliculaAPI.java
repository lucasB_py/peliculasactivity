package com.trash.peliculasactivity.utils;

import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.response.BuscarPeliculaResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PeliculaAPI {

    //Buscar peliculas
    @GET("3/search/movie")
    Call<BuscarPeliculaResponse> buscarPelicula(
            @Query("api_key") String key,
            @Query("query") String query,
            @Query("page") int page
    );

    //Obtener las peliculas populares
    @GET("/3/movie/popular")
    Call<BuscarPeliculaResponse> getPopular(
            @Query("api_key") String key,
            @Query("page") int page

    );

    //Buscar peliculas x su ID
    @GET("3/movie/{movie_id}?")
    Call<PeliculaModelo> getPelicula(
            @Path("movie_id") int movie_id,
            @Query("api_key") String key
    );
}
