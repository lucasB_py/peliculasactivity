package com.trash.peliculasactivity.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.request.PeliculaApiClient;

import java.util.List;

public class PeliculaRepository {

    private static PeliculaRepository instance;

    private PeliculaApiClient peliculaApiClient;

    private String mQuery;
    private int mPageNumber;

    public static PeliculaRepository getInstance(){
        if (instance == null){
            instance = new PeliculaRepository();
        }
        return instance;
    }

    private PeliculaRepository(){
        peliculaApiClient = PeliculaApiClient.getInstance();
    }

    public LiveData<List<PeliculaModelo>> getPeliculas(){
        return peliculaApiClient.getPeliculas();
    }

    public LiveData<List<PeliculaModelo>> getPeliculasPop(){
        return peliculaApiClient.getPeliculasPop();
    }

    //Llamando al metodo
    public void buscarPeliculaApi(String query, int pageNumber){
        mQuery = query;
        mPageNumber = pageNumber;
        peliculaApiClient.buscarPeliculasApi(query, pageNumber);
    }

    public void buscarPeliculaPopApi(int pageNumber){
        mPageNumber = pageNumber;
        peliculaApiClient.buscarPeliculasPopApi(pageNumber);
    }

    public void buscarSiguientePagina(){
        buscarPeliculaApi(mQuery, mPageNumber + 1);
    }
}

