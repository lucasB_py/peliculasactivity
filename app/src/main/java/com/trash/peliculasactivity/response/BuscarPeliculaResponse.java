package com.trash.peliculasactivity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.trash.peliculasactivity.models.PeliculaModelo;

import java.util.List;

public class BuscarPeliculaResponse {

    @SerializedName("total_results")
    @Expose()
    private int total_count;

    @SerializedName("results")
    @Expose()
    private List<PeliculaModelo> peliculas;

    public int getTotal_count() {
        return total_count;
    }

    public List<PeliculaModelo> getPeliculas() {
        return peliculas;
    }

    @Override
    public String toString() {
        return "BuscarPeliculaResponse{" +
                "total_count=" + total_count +
                ", peliculas=" + peliculas +
                '}';
    }
}
