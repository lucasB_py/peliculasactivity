package com.trash.peliculasactivity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.trash.peliculasactivity.models.PeliculaModelo;

public class PeliculaResponse {

    @SerializedName("results")
    @Expose
    private PeliculaModelo peliculaModelo;

    public PeliculaModelo getPeliculaModelo() {
        return peliculaModelo;
    }

    @Override
    public String toString() {
        return "PeliculaResponse{" +
                "peliculaModelo=" + peliculaModelo +
                '}';
    }
}
