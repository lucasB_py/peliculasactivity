package com.trash.peliculasactivity.adapters;

public interface OnPeliculaListener {

    void onPeliculaClick(int position);

    void onCategoriaClick(String category);

}
