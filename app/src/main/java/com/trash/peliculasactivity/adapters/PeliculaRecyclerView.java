package com.trash.peliculasactivity.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.trash.peliculasactivity.R;
import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.utils.Credenciales;

import java.util.List;

public class PeliculaRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PeliculaModelo> mPeliculas;
    private OnPeliculaListener onPeliculaListener;

    private static final int DISPLAY_POP = 1;
    private static final int DISPLAY_SEARCH = 2;

    public PeliculaRecyclerView(OnPeliculaListener onPeliculaListener) {
        this.onPeliculaListener = onPeliculaListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == DISPLAY_SEARCH){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pelicula_list_item,
                    parent, false);
            return new PeliculaViewHolder(view, onPeliculaListener);
        }
        else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_layout,
                    parent, false);
            return new PopularViewHolder(view, onPeliculaListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        int itemViewType = getItemViewType(i);

        if (itemViewType == DISPLAY_SEARCH){

            ((PeliculaViewHolder)holder).ratingBar.setRating(mPeliculas.get(i)
                    .getVote_average() / 2);

            //ImageView usando la libreria Glide
            Glide.with(holder.itemView.getContext())
                    .load("https://image.tmdb.org/t/p/w500/"  + mPeliculas.get(i)
                            .getPoster_path())
                    .into(((PeliculaViewHolder)holder).imageView);
        } else {
            ((PopularViewHolder)holder).ratingBar_pop.setRating(mPeliculas.get(i)
                    .getVote_average() / 2);

            //ImageView usando la libreria Glide
            Glide.with(holder.itemView.getContext())
                    .load("https://image.tmdb.org/t/p/w500/"  + mPeliculas.get(i)
                            .getPoster_path())
                    .into(((PopularViewHolder)holder).imageView_pop);

        }
    }

    @Override
    public int getItemCount() {
        if (mPeliculas != null) {
            return mPeliculas.size();
        }
        return 0;
    }

    public void setmPeliculas(List<PeliculaModelo> mPeliculas) {
        this.mPeliculas = mPeliculas;
        notifyDataSetChanged();
    }

    //Obteniendo el ID de la pelicula seleccionada
    public PeliculaModelo getPeliculaSeleccionada(int position){
        if (mPeliculas != null){
            if (mPeliculas.size() > 0){
                return mPeliculas.get(position);
            }
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        if (Credenciales.POPULAR){
            return DISPLAY_POP;
        } else {
            return DISPLAY_SEARCH;
        }
    }
}
