package com.trash.peliculasactivity.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.trash.peliculasactivity.R;

public class PeliculaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    //Widgets de pelicula_list_item.xml
    ImageView imageView;
    RatingBar ratingBar;

    //Click Listener
    OnPeliculaListener onPeliculaListener;

    public PeliculaViewHolder(@NonNull View itemView, OnPeliculaListener onPeliculaListener) {
        super(itemView);

        this.onPeliculaListener = onPeliculaListener;

        imageView = itemView.findViewById(R.id.movie_img);
        ratingBar = itemView.findViewById(R.id.rating_bar);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onPeliculaListener.onPeliculaClick(getAdapterPosition());
    }
}
