package com.trash.peliculasactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.trash.peliculasactivity.adapters.OnPeliculaListener;
import com.trash.peliculasactivity.adapters.PeliculaRecyclerView;
import com.trash.peliculasactivity.models.PeliculaModelo;
import com.trash.peliculasactivity.request.Servicey;
import com.trash.peliculasactivity.response.BuscarPeliculaResponse;
import com.trash.peliculasactivity.utils.Credenciales;
import com.trash.peliculasactivity.utils.PeliculaAPI;
import com.trash.peliculasactivity.viewmodels.PeliculaListaViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PeliculasListActivity extends AppCompatActivity implements OnPeliculaListener {

    //RecyclerView
    private RecyclerView recyclerView;
    private PeliculaRecyclerView peliculaRecyclerAdapter;

    //ViewModel
    private PeliculaListaViewModel peliculaListaViewModel;

    boolean esPopular = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //SearchView
        SetupSearchView();

        recyclerView = findViewById(R.id.recyclerView);
        peliculaListaViewModel = new ViewModelProvider(this).get(PeliculaListaViewModel.class);

        ConfigureRecyclerView();
        ObserveAnyChange();
        ObvervePeliculasPopulare();

        //Obteniendo las peliculas populares
        peliculaListaViewModel.buscarPeliculaPopApi(1);

        Log.v("TAG", "is pop: " + esPopular);
        
    }

    private void ObvervePeliculasPopulare() {
        peliculaListaViewModel.getPeliculasPop().observe(this, new Observer<List<PeliculaModelo>>() {
            @Override
            public void onChanged(List<PeliculaModelo> peliculaModelos) {
                if (peliculaModelos != null){
                    for (PeliculaModelo peliculaModelo : peliculaModelos){
                        //Obtener la informacion
                        peliculaRecyclerAdapter.setmPeliculas(peliculaModelos);
                    }
                }
            }
        });
    }

    //Observar cualquier cambio en la informacion
    private void ObserveAnyChange(){
        peliculaListaViewModel.getPeliculas().observe(this, new Observer<List<PeliculaModelo>>() {
            @Override
            public void onChanged(List<PeliculaModelo> peliculaModelos) {
                if (peliculaModelos != null){
                    for (PeliculaModelo peliculaModelo : peliculaModelos){
                        //Obtener la informacion
                        peliculaRecyclerAdapter.setmPeliculas(peliculaModelos);
                    }
                }
            }
        });
    }

    //Inicializando el RecyclerView y agregando data en el
    private void ConfigureRecyclerView(){
        peliculaRecyclerAdapter = new PeliculaRecyclerView(this);

        recyclerView.setAdapter(peliculaRecyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL,
                false));

        //Cargar pagina siguiente de la api
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)){
                    peliculaListaViewModel.buscarSiguientePagina();
                }
            }
        });
    }

    @Override
    public void onPeliculaClick(int position) {
        //Toast.makeText(this, "Posición: " + position, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, PeliculaDetalles.class);
        intent.putExtra("pelicula", peliculaRecyclerAdapter.getPeliculaSeleccionada(position));
        startActivity(intent);
    }

    @Override
    public void onCategoriaClick(String category) {

    }

    //Obtener query del usuario
    private void SetupSearchView() {
        final SearchView searchView = findViewById(R.id.seach_view);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                esPopular = false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                peliculaListaViewModel.buscarPeliculaApi(
                        query,
                        1
                );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


    }


    /*private void GetRetrofitResponse() {
        PeliculaAPI peliculaAPI = Servicey.getPeliculaAPI();
        Call<BuscarPeliculaResponse> responseCall = peliculaAPI
                .buscarPelicula(
                        Credenciales.API_KEY,
                        "Action",
                        1
                );

        responseCall.enqueue(new Callback<BuscarPeliculaResponse>() {
            @Override
            public void onResponse(Call<BuscarPeliculaResponse> call, Response<BuscarPeliculaResponse> response) {
                if (response.code() == 200){
                    //Log.v("TAG","Response" + response.body().toString());
                    List<PeliculaModelo> peliculas = new ArrayList<>(response.body().getPeliculas());

                    for (PeliculaModelo pelicula : peliculas){
                        Log.v("TAG", "Release date: " + pelicula.getTitle());
                    }
                } else {
                    try {
                        Log.v("TAG", "Error" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BuscarPeliculaResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    private void GetRetrofitResponseAccordingToID(){
        PeliculaAPI peliculaAPI = Servicey.getPeliculaAPI();
        Call<PeliculaModelo> responseCall = peliculaAPI
                .getPelicula(
                        343611,
                        Credenciales.API_KEY
                );

        responseCall.enqueue(new Callback<PeliculaModelo>() {
            @Override
            public void onResponse(Call<PeliculaModelo> call, Response<PeliculaModelo> response) {

                if(response.code() == 200){
                    PeliculaModelo pelicula = response.body();
                    Log.v("TAG", "The response " + pelicula.getTitle());
                } else {
                    try {
                        Log.v("TAG", "Error " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PeliculaModelo> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }*/
}