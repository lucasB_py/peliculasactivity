package com.trash.peliculasactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trash.peliculasactivity.models.PeliculaModelo;

public class PeliculaDetalles extends AppCompatActivity {

    //Widgets
    private ImageView imageViewDetails;
    private TextView titleDetails, descDetails;
    private RatingBar ratingBarDetails;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicula_detalles);
        
        imageViewDetails = findViewById(R.id.imageView_details);
        titleDetails = findViewById(R.id.textView_title_details);
        descDetails = findViewById(R.id.textView_desc_details);
        ratingBarDetails = findViewById(R.id.ratingBar_details);
        
        GetDataFromIntent();
    }

    private void GetDataFromIntent() {
        if (getIntent().hasExtra("pelicula")){
            PeliculaModelo peliculaModelo = getIntent().getParcelableExtra("pelicula");
            titleDetails.setText(peliculaModelo.getTitle());
            descDetails.setText(peliculaModelo.getMovie_overview());
            ratingBarDetails.setRating(peliculaModelo.getVote_average() / 2);

            Log.v("TAG", peliculaModelo.getMovie_overview());

            Glide.with(this)
                    .load("https://image.tmdb.org/t/p/w500/" + peliculaModelo.getPoster_path())
                    .into(imageViewDetails);
        }
    }

}